cmake_minimum_required(VERSION 3.16)
project(linear_sort)

set(CMAKE_C_FLAGS "-Wall -Wextra -O3")

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_FLAGS "-Wall -Wextra -O3")

add_executable(linear_sort_cpp cxximpl/main.cpp cxximpl/linear_sort.cpp)
add_executable(linear_sort_c cimpl/main.c cimpl/linear_sort.c)