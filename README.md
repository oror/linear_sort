# Simple linear sort

## Summary

Coding Task-Abstract
Sort an array of 0s, 1s and 2s in linear time complexity, like this one:
1 into [30] = { 2, 2, 1, 0, 1, 0, 1, 1, 0, 2, 2, 1, 1, 1, 2, 0, 0, 1, 1, 0, 1, 0, 1, 2, 0, 0, 0,1, 1, 2};

Given an array consisting only 0s, 1s and 2s, please provide an algorithm sorting that array in
O(n) time complexity. So in the resulting sorted array, 0s will be at starting, then the 1s & then the
2s.

● The usage of built-ins or libraries, like std::sort or boost::sort is not allowed.

● Coding style and quality is more important than feature completeness.

● What to submit: A git repository of the work and all commits made on the way.

## Implementation

This task could be solved with application of counting sort algorithm.
Since value set is pretty narrow {0,1,2}, it is possible to count all occurrences of these values across array in linear time.
Sorted array is also recreated in linear time based on number of occurrences.
In the end we have 2 * O(n) which is falling under O(n) time complexity.

There are two very similar implementations in C and C++ style, both are compiled into two separate binaries.

##p.s.

Sorting of this kind of predefined arrays could be done in O(1) runtime complexity.
Starting from C++20 with constexpr std::sort or even with earlier standards it is possible to sort it during compile time.
