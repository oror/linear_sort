/*
 * Developed by Ihar Rahatka, May 2020
 */

#ifndef LINEAR_SORT_LINEAR_SORT_HPP
#define LINEAR_SORT_LINEAR_SORT_HPP

namespace linear_sort
{

constexpr auto RANGE_MIN = 0;   // minimum possible value of an array element
constexpr auto RANGE_MAX = 2;   // maximum possible value of an array element

/**
 * @brief Sorts a set of {RANGE_MIN ... RANGE_MAX} ints within range between begin and end in O(n) time.
 * @param begin First pointer of a range.
 * @param end Last pointer of a range.
 */
void linearSort(int* begin, int* end);

/**
 * @brief Prints contents of an array.
 * @param begin First pointer of a range.
 * @param end Last pointer of a range.
 */
void print(const int* begin, const int* end);

} //namespace linear_sort
#endif //LINEAR_SORT_LINEAR_SORT_HPP
