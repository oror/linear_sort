/*
 * Developed by Ihar Rahatka, May 2020
 */

#include "linear_sort.hpp"

#include <iostream> // for std::cout
#include <array> // for std::array

namespace linear_sort
{

void linearSort(int *begin, int *end)
{
    static_assert(RANGE_MAX > RANGE_MIN);
    constexpr auto range = RANGE_MAX - RANGE_MIN + 1;

    auto throwIfNotInRange = [](auto &&value) -> int
    {
        if (value < RANGE_MIN || value > RANGE_MAX)
        {
            throw std::runtime_error("illegal array element: " + std::to_string(value));
        }
        return value;
    };

    if (begin == nullptr || end == nullptr)
    {
        throw std::runtime_error("either begin or end pointer is null");
    }

    std::array<int, range> count{};

    for (auto ptr = begin; ptr != end; ++ptr)
    {
        ++count[throwIfNotInRange(*ptr)];
    }

    auto ptr = begin;
    for (size_t index = 0; index < range; ++index)
    {
        while (count[index]-- != 0)
        {
            *(ptr++) = index;
        }
    }
}

void print(const int* begin, const int* end)
{
    if (begin == nullptr || end == nullptr)
    {
        throw std::runtime_error("either begin or end pointer is null");
    }

    for (auto ptr = begin; ptr != end; ++ptr)
    {
        std::cout << *ptr << " ";
    }
    std::cout << std::endl;
}

} //namespace linear_sort

