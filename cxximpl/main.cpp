/*
 * Developed by Ihar Rahatka, May 2020
 */

#include "linear_sort.hpp"

#include <algorithm> // for for_each
#include <cassert> // for assert
#include <chrono> // for chrono
#include <iostream> // for std::cout

int main()
{
    // sorted array to compare with
    constexpr int arrSorted[30] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2};

    // array to sort
    int arr [30] = {2, 2, 1, 0, 1, 0, 1, 1, 0, 2, 2, 1, 1, 1, 2, 0, 0, 1, 1, 0, 1, 0, 1, 2, 0, 0, 0, 1, 1, 2};

    try
    {
        std::cout << "linear_sort_cpp\nBefore sort:\n";
        linear_sort::print(std::cbegin(arr), std::cend(arr));

        // unfortunately applying std::begin on C-style array returns just a pointer
        // therefore I make signature of linearSort() to accept C-style pointers and not proper iterators
        linear_sort::linearSort(std::begin(arr), std::end(arr));

        std::cout << "After sort:\n";
        linear_sort::print(std::cbegin(arr), std::cend(arr));

        assert(std::equal(std::cbegin(arr), std::cend(arr), std::cbegin(arrSorted)));

        // let's play with somewhat bigger arrays and measure time of sorting between linear_sort and std::sort

        int linearSortArray [UINT16_MAX] {};    // placeholder for linear_sort()
        int stdSortArray [UINT16_MAX] {};       // placeholder for std::sort

        // fill array with pseudorandom values in range {0,1,2}
        std::for_each(std::begin(linearSortArray), std::end(linearSortArray),
                [] (auto& value) { value = rand() % 3; });

        // make two arrays identical
        std::copy(std::cbegin(linearSortArray), std::cend(linearSortArray), std::begin(stdSortArray));

        // execute functions and capture timepoints
        auto tp1 = std::chrono::high_resolution_clock::now();
        linear_sort::linearSort(std::begin(linearSortArray), std::end(linearSortArray));
        auto tp2 = std::chrono::high_resolution_clock::now();
        std::sort(std::begin(stdSortArray), std::end(stdSortArray));
        auto tp3 = std::chrono::high_resolution_clock::now();

        auto linearSortUs = std::chrono::duration_cast<std::chrono::microseconds>(tp2 - tp1);
        auto stdSortUs = std::chrono::duration_cast<std::chrono::microseconds>(tp3 - tp2);

        assert(std::equal(std::cbegin(stdSortArray), std::cend(stdSortArray), std::cbegin(linearSortArray)));

        std::cout << "Linear sort took " << linearSortUs.count() << " microseconds\n"
        << "std::sort took " << stdSortUs.count() << " microseconds\n";
    }
    catch (const std::exception& exc)
    {
        std::cerr << exc.what();
        return -1;
    }
    return 0;
}
