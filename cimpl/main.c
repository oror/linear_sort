/*
 * Developed by Ihar Rahatka, May 2020
 */

#include "linear_sort.h"

#include <stdio.h>  // for printf
#include <string.h> // for memcmp

int main()
{
    // sorted array to compare with
    const int arrSorted[30] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2};

    // array to sort
    int arr [30] = {2, 2, 1, 0, 1, 0, 1, 1, 0, 2, 2, 1, 1, 1, 2, 0, 0, 1, 1, 0, 1, 0, 1, 2, 0, 0, 0, 1, 1, 2};

    // number of elements in array
    size_t arrSize = sizeof(arr) / sizeof(arr[0]);

    printf("linear_sort_c\nBefore sort:\n");
    print(arr, arrSize);

    if (linearSort(arr, arrSize) == 0)
    {
        printf("After sort:\n");
        print(arr, arrSize);
    }
    if (memcmp(arr, arrSorted, arrSize) == 0)
    {
        return 0;
    }
    else
    {
        fprintf(stderr, "sort failed");
        return -1;
    }
}
