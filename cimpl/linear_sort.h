/*
 * Developed by Ihar Rahatka, May 2020
 */

#ifndef LINEAR_SORT_LINEAR_SORT_H
#define LINEAR_SORT_LINEAR_SORT_H

#include <stddef.h> // for NULL and size_t

/**
 * @brief Sorts a set of {0, 1, 2} values in array in O(n) time.
 * @param arr Pointer to the first element of array.
 * @param size Size of array.
 */
int linearSort(int* const arr, const size_t size);

/**
 * @brief Prints contents of array.
 * @param arr Pointer to the first element of array.
 * @param size Size of array.
 */
void print(int* const arr, const size_t size);

#endif //LINEAR_SORT_LINEAR_SORT_H
