/*
 * Developed by Ihar Rahatka, May 2020
 */

#include "linear_sort.h"

#include <stdio.h>  // for printf

int linearSort(int* const arr, const size_t size)
{
    if (arr == NULL)
    {
        fprintf(stderr, "array is null");
        return -1;
    }
    size_t zeros = 0;
    size_t ones = 0;
    size_t twos = 0;

    int* ptr = arr;
    for (size_t i = 0; i < size; ++i, ++ptr)
    {
        if (*ptr == 0)
        {
            ++zeros;
        }
        else if (*ptr == 1)
        {
            ++ones;
        }
        else if (*ptr == 2)
        {
            ++twos;
        }
        else
        {
            fprintf(stderr, "illegal array element: %d", *ptr);
            return -1;
        }
    }

    ptr = arr;
    while (zeros-- != 0)
    {
        *(ptr++) = 0;
    }
    while (ones-- != 0)
    {
        *(ptr++) = 1;
    }
    while (twos-- != 0)
    {
        *(ptr++) = 2;
    }

    return 0;
}

void print(int* const arr, const size_t size)
{
    if (arr == NULL)
    {
        fprintf(stderr, "array is null");
    }
    for (size_t i = 0; i < size; ++i)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
}

